from django.contrib import admin
from example.models import *
# Register your models here.

admin.site.register(Actor)
admin.site.register(FilmCategory)
admin.site.register(Film)
admin.site.register(Rental)
admin.site.register(FilmActor)
admin.site.register(Inventory)
admin.site.register(Customer)

