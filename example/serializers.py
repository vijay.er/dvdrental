from rest_framework import serializers
from example.models import *


class FilmSerializer(serializers.ModelSerializer):
    class Meta:
        model = Film
        fields = ('film_id', 'title',)


class ActorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Actor
        fields = ['actor_id', 'first_name', 'last_name']


class FilmActorSerializer(serializers.ModelSerializer):
    class Meta:
        model = FilmActor
        fields = '__all__'


class FilmCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = FilmCategory
        fields = '__all__'


class RetriveCategorySerializer(serializers.ModelSerializer):
    fmfilms = FilmSerializer(many=True, read_only=True)

    class Meta:
        model = Actor
        fields = ['first_name', 'last_name', 'fmfilms',]


class RentalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rental
        fields = '__all__'


class InventorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Inventory
        fields = '__all__'
