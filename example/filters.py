from .models import *
import django_filters


class FilmFilters(django_filters.FilterSet):
    # make = django_filters.ModelChoiceFilter(field_name='actor', queryset=FilmActor.objects.all())

    class Meta:
        model = FilmActor
        fields = ['actor', ]
