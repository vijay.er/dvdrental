from example.serializers import *
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import generics
from example.filters import *


class FilmList(generics.ListAPIView):
    queryset = Film.objects.all()
    serializer_class = FilmSerializer


class ActorList(generics.ListAPIView):
    queryset = Actor.objects.all()
    serializer_class = ActorSerializer


@api_view(['GET'])
def movie(request, **args):
    actor = request.GET.get('id')
    data = Film.objects.filter(fmfilms__actor_id=actor)
    serializer = FilmSerializer(data, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def category(request, **args):
    category = request.GET.get('id')
    data = Actor.objects.filter(fmactors__film__filmcategory=category)
    serializer = RetriveCategorySerializer(data, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def rental(request, **args):
    id = request.GET.get('id')
    film = Film.objects.values_list('film_id', flat=True).filter(fmfilms__actor__actor_id=id)
    rentalid = Rental.objects.values_list('rental_id', flat=True).filter(inventory__film__film_id__in=film)
    filmtitel = Film.objects.filter(inventories__rentalinventory__rental_id__in=rentalid).distinct('title')
    serializer = FilmSerializer(filmtitel, many=True)
    return Response(serializer.data)


# class MovieListView(generics.ListAPIView):
#     queryset = FilmActor.objects.all()
#     serializer_class = FilmActorSerializer
#     filter_backends = (filters.DjangoFilterBackend,)
#     filter_class = FilmFilters