from django.urls import path
from . import views
from .views import *

urlpatterns = [
    path('filmlist/', FilmList.as_view(), name='filmlist'),
    path('actorlist/', ActorList.as_view(), name='actorlist'),
    path('movie/', views.movie, name='movie'),
    path('category/', views.category, name='category'),
    path('rental/', views.rental, name='rental'),
    # path('movielist/', MovieListView.as_view(), name='movielist'),

]
